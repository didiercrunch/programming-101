{
  description = "Programming 101";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs?ref=nixpkgs-unstable";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils }: (utils.lib.eachDefaultSystem (system: rec {

    pkgs = nixpkgs.legacyPackages.${system};
    pythonEnv = pkgs.python3.withPackages(ps: with ps; [ numpy
                                                           pandas
                                                           jupyter
                                                           requests
                                                           avro
                                                           matplotlib ] );
    presentation = pkgs.stdenv.mkDerivation {
      name = "slides";
      buildInputs = [pythonEnv];
      src = ./.;
      buildPhase = ''
      echo "bob"
      ${pythonEnv}/bin/jupyter nbconvert objets-et-heritage.ipynb --to slides
      '';
      installPhase = ''
      mkdir -p $out
      cp -r images $out
      cp objets-et-heritage.slides.html $out/index.html
      '';
    };
    packages = {
      pythonEnv = pythonEnv;
      presentation = presentation;
    };

    defaultPackage = pythonEnv; # If you want to juist build the environment
    devShell = pythonEnv.env; # We need .env in order to use `nix develop`
  }));
}

